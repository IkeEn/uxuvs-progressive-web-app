'use strict';

/**
 * Function to load the new content (need a rework)
 */
var categorie = "IT";

function refreshContentOne() {

    $('#container').empty();
    $("#IT").addClass('active');
    $("#Hiking").removeClass();
    $("#Music").removeClass();
    $.get('source.html', function (data) {
        $('#content').html(data);
        categorie = "IT";
        loadData()
    });
}

function refreshContentTwo() {

    $('#container').empty();
    $("#Music").addClass('active');
    $("#Hiking").removeClass();
    $("#IT").removeClass();
    $.get('source.html', function (data) {
        $('#content').html(data);
        categorie = "Music";
        loadData()
    });
}

function refreshContentThree() {

    $('#container').empty();
    $("#Hiking").addClass('active');
    $("#Music").removeClass();
    $("#IT").removeClass();
    $.get('source.html', function (data) {
        $('#content').html(data);
        categorie = "Hiking";
        loadData()
    });
}

/**
 * API request and DIV filling
 */
function loadData(){

    try {
        var container = document.querySelector('#container');
    } catch (e) {

    }

    fetch('http://contentpool.fateyedive-records.de/api/categories.json?name='+categorie)
        .then((resp) => resp.json())
        .then(function (data) {
            let json = data;
            var posts = json[0]['posts'];

            for (var i = 0; i < posts.length; i++) {

                container.innerHTML += '<div class="row">' +
                    '<img src="http://contentpool.fateyedive-records.de/uploads/images/posts/' + posts[i].image + '">' +
                    '<div class="twelve column post--title">' + posts[i].title + '</div>' +
                    '<div class="twelve column post--body">' + posts[i].body + '</div>' +
                    '</div>';
            }
        })
        .catch(function (error) {
                console.log('Looks like there was a problem: \n', error);

        });

}

function refreshall(){
    //remove the old div and the old data
    $('#container').empty();
    refreshContentOne();
}
